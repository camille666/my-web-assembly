#include <emscripten.h>
#include <iostream>
using namespace std;

int main() {
  int val1 = 21;
  int val2 = EM_ASM_INT({ return $0 * 2; }, val1);
  cout<<"val2 == "<< val2 << endl;
}
